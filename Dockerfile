FROM python:3.8

RUN set -ex; \
	apt-get update; \
	apt-get install -y --no-install-recommends tesseract-ocr; \
	rm -rf /var/lib/apt/lists/*

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt
RUN pyppeteer-install
