var google_maps_base_url = 'https://www.google.com/maps/search/?api=1&query='
var ics_url = "events.ics";

document.addEventListener('DOMContentLoaded', function () {
    var calendar = new FullCalendar.Calendar(document.getElementById('calendar'), {
        initialView: 'listYear',
        locale: 'de',
        height: 'auto',
        customButtons: {
            icsDownload: {
                text: 'Download ICS',
                click: function () {
                    var href = window.location.href;
                    var dir = href.substring(0, href.lastIndexOf('/')) + "/";
                    location = dir + ics_url;
                }
            }
        },
        headerToolbar: {
          right: 'icsDownload today prev,next',
        },
        eventContent: function(arg) {
            var title = document.createElement('div');
            title.innerHTML = arg.event.title;
            var link = document.createElement('div');
            link.innerHTML = linkifyStr(
                arg.event.extendedProps.icalEvent.description,
                {
                    nl2br: true,
                    format: {
                        url: function (value) {
                            if (value.includes('http://www.fussball.de')) return 'FUSSBALL.DE'
                            if (value.includes('https://www.google.com/maps')) return 'Google Maps'
                            return value.length > 50 ? value.slice(0, 50) + '…' : value
                        }
                    }
                }
            );
            return { domNodes: [title, link] }
        },
        events: function (info, successCallback, failureCallback) {
            var req = new XMLHttpRequest();
            req.addEventListener("load", function () {
                var jcalData = ICAL.parse(this.responseText);
                var vcalendar = new ICAL.Component(jcalData);
                var vevents = vcalendar.getAllSubcomponents("vevent");
                var events = vevents.map(function (vevent) {
                    var e = new ICAL.Event(vevent);
                    return {
                        title: e.summary,
                        start: e.startDate.toJSDate(),
                        end: e.endDate.toJSDate(),
                        extendedProps: {
                            icalEvent: e
                        }
                    }
                });
                successCallback(events);
            });
            req.open("GET", ics_url);
            req.send();
        }
    });
    calendar.render();
});
