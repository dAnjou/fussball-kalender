image:
	docker build --tag fussball-kalender .

run: image
	docker run --workdir "/app" --volume "$(shell pwd):/app" fussball-kalender python3 main.py
