import json
from datetime import timedelta, datetime
from string import Template
import itertools
from urllib.parse import quote_plus

import bs4
from dateutil import parser, tz
import requests
from ics import Calendar, Event

URL_PATTERN = Template(
    'http://www.fussball.de/ajax.team.matchplan/-'
    '/mime-type/JSON'
    '/mode/PAGE'
    '/prev-season-allowed/false'
    '/show-filter/false'
    '/team-id/${team_id}'
    '/max/${max_entries}'
    '/datum-von/${from_date}'
    '/datum-bis/${to_date}'
    '/show-venues/checked'
    '/offset/${offset}'
)

now = datetime.utcnow()


def extract(row_set):
    cols = row_set[1].find(class_='column-detail')
    url = (cols.find('a') or {}).get('href', '')
    if not url:
        return None  # because it's not a real game then

    cols = row_set[0].find_all('td')
    description = cols[2].get_text().strip()

    dt = parser.parse(cols[0].get_text(), dayfirst=True, fuzzy=True)
    dt = dt.replace(tzinfo=tz.gettz('Europe/Berlin'))

    cols = row_set[1].find_all(class_='column-club')
    encounter = " : ".join(c.get_text().strip() for c in cols)

    location = row_set[2].get_text().strip() if len(row_set) == 3 else None
    maps_url = None
    if location:
        location_query = quote_plus(location)
        maps_url = f'https://www.google.com/maps/search/?api=1&query={location_query}'

    return Event(
        name=encounter,
        created=now,
        begin=dt,
        end=dt + timedelta(hours=2),
        location=location,
        description='\n\n'.join(filter(None, [description, url, maps_url])),
        url=url
    )


def main(team_id, from_date, to_date):
    cal = Calendar()
    offset = 0
    max_entries = 10
    url_pattern = Template(URL_PATTERN.safe_substitute(
        team_id=team_id,
        from_date=from_date,
        to_date=to_date,
        max_entries=max_entries
    ))

    while True:
        url = url_pattern.safe_substitute(offset=offset)
        response = requests.get(url=url)
        result = json.loads(response.content)
        soup = bs4.BeautifulSoup(markup=result['html'], features='html.parser')

        for is_headline_row, row_set in itertools.groupby(
                soup.tbody.find_all('tr'),
                key=lambda row: 'row-headline' in row.get('class', [])
        ):
            if is_headline_row:
                continue
            event = extract(list(row_set))
            cal.events.add(event)

        if result['final']:
            break
        offset += max_entries

    cal.events.discard(None)

    with open(file=f'public/events.ics', mode='w') as f:
        f.writelines(cal)


if __name__ == '__main__':
    from_date = now.date() - timedelta(weeks=4)
    to_date = now.date() + timedelta(weeks=6 * 4)
    main(
        team_id='027I7PSI1G000000VS5489B1VUA37ON8',
        from_date=from_date,
        to_date=to_date
    )
